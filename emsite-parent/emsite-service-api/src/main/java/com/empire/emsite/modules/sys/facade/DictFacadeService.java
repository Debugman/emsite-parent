/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.sys.facade;

import java.util.List;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.sys.entity.Dict;

/**
 * 类DictFacadeService.java的实现描述：字典FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:51:43
 */
public interface DictFacadeService {

    /**
     * 查询字段类型列表
     * 
     * @return
     */
    public List<String> findTypeList();

    public void save(Dict dict);

    public void delete(Dict dict);

    public Dict get(String id);

    public Page<Dict> findPage(Page<Dict> page, Dict dict);

    public List<Dict> findList(Dict dict);

    public List<Dict> findAllList();
}
