/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.gen.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.validator.constraints.Length;

import com.empire.emsite.common.persistence.DataEntity;
import com.empire.emsite.common.utils.StringUtils;
import com.google.common.collect.Lists;

/**
 * 类GenTemplate.java的实现描述：生成方案Entity
 * 
 * @author arron 2017年10月30日 下午12:58:34
 */
@XmlRootElement(name = "template")
public class GenTemplate extends DataEntity<GenTemplate> {

    private static final long serialVersionUID = 1L;
    private String            name;                 // 名称
    private String            category;             // 分类
    private String            filePath;             // 生成文件路径
    private String            fileName;             // 文件名
    private String            content;              // 内容

    public GenTemplate() {
        super();
    }

    public GenTemplate(String id) {
        super(id);
    }

    @Length(min = 1, max = 200)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @XmlTransient
    public List<String> getCategoryList() {
        if (category == null) {
            return Lists.newArrayList();
        } else {
            return Lists.newArrayList(StringUtils.split(category, ","));
        }
    }

    public void setCategoryList(List<String> categoryList) {
        if (categoryList == null) {
            this.category = "";
        } else {
            this.category = "," + StringUtils.join(categoryList, ",") + ",";
        }
    }

}
